#!/usr/bin/env python
#
# Front end GUI for eqcut
#
################################################################
#
# tkeqcut
#
# created: 08/23/2000
# version: 2000.236
# author: Bruce Beaudoin
#
################################################################
#
# modification: 08/24/2000
# version: 2000.237
# author: Bruce Beaudoin
#
# input parameter and file error checking
# added help
#
################################################################
#
# modification: 09/12/2000
# version: 2000.256
# author: Bruce Beaudoin
#
# introduced ability to set tolerance
# introduced ability provide pcf file for time corrections
#
################################################################
#
# modification: 03/19/2002
# version: 2002.091
# author: Bruce Beaudoin
#
# modified to work with eqcut.pl 2002.091
#
################################################################
#
# modification: 03/19/2002
# version: 2002.109
# author: Bruce Beaudoin
#
# port to python
#
################################################################
#
# modification: 08/21/2002
# version: 2002.233
# author: Bruce Beaudoin
#
# finished port to python
#

#
# annotate globals and in general code
# add a button for dasfile nb to build from traces (like in the early version
# consistancy in error handling
#
################################################################
#
# modification: 08/21/2002
# version: 2002.233a
# author: Bruce Beaudoin
#
# cleaned up annoying print(statements
#
################################################################
#
# modification: 08/26/2002
# version: 2002.238
# author: Bruce Beaudoin
#
# fixed re.sub statement fixing PASSCAL path
# modified to work with new generalized waveform search
# deprecated data stream
#
# version: 2002:240
# finished version 238 fixes
#
################################################################
#
# modification: 09/13/2002
# version: 2002.256
# author: Bruce Beaudoin
#
# fixed bug in testFileFormat try:except statements for das and evt file
# opening
################################################################
#
# modification: 10/09/2002
# version: 2002.282
# author: Bruce Beaudoin
#
# added ability to dump cutEvents output to file
# added ability to input sample rate(s) to cut
#
################################################################
#
# modification: 02/08/2003
# version: 2003.038
# author: Bruce Beaudoin
#
# modified version to coincide with eqcut.py update
# that incorporates rt130 hex serial number
#
################################################################
#
# modification: 05/15/2003
# version: 2003.135
# author: Bruce Beaudoin
#
# introduced Popen3 for running external jobs so we can show progress
# fixed checkInput so it wouldn't throw an error when IntVars undef

################################################################
#
# modification
# version: 2005.045
# author: Bruce Beaudoin
#
# Major Rewrite
# folded eqcut.py into tkeqcut.py
# optimized cutting routine.
# optimized file finding routine
# rethought data file dictionary. It is now {das:{chan:[(rate, epoch, filetype,
# fullname)]}}
# now can create a das:chan list from the data file dictionary
# Progress bar and EventCutText (log window) now properly update during
# execution
# EventCutText log window now uses color to make it more readable
# modularized many routines

################################################################
#
# modification
# version: 2005.048
# author: Bruce Beaudoin
#
# self.Tolerance was not cast as string in RunProg. Fixed
################################################################
#
# modification
# version: 2007.030
# author: Bruce Beaudoin
#
# modified to handle location codes
#
################################################################
#
# modification
# version: 2008.180
# author: Bruce Beaudoin
#
# modified to take advantage of LibTrace optimization
#
################################################################
#
# modification
# version: 2019.028
# author: Lan Dam
#
# convert to be compatible with both py2 and py3
#
################################################################
#
# modification
# version: 2020.216
# author: Maeva Pourpoint
#
# Updates to work under Python 3.
# Unit tests to ensure basic functionality.
# Code cleanup to conform to the PEP8 style guide.
# Directory cleanup (remove unused files introduced by Cookiecutter).
# Packaged with conda.
################################################################
#
# modification
# version: 2022.1.0.0
# author: Maeva Pourpoint
#
# Update versioning scheme
################################################################

import glob
import os
import Pmw
import re
import sys
import time

from operator import mod
from subprocess import *
from tkinter import *
from tkinter.filedialog import *

from tkeqcut.TimeConvert import CalDay, Year2To4, Year4To2
from tkeqcut.LibTrace import *

VERSION = "2022.1.0.0"


# From "Python and Tkinter Programming", J.E. Grayson, pg.103
########################################


class Command:
    def __init__(self, func, *args, **kw):
        self.func = func
        self.args = args
        self.kw = kw

    def __call__(self, *args, **kw):
        args = self.args + args
        kw.update(self.kw)
        self.func(*args, **kw)

#
#    Over ride some methods in FileDialog so we only select directories
#    From TraceBuilder.py, auth Steve Azevedo & Lloyd Carothers
#
########################################


class DirectoryDialog(FileDialog):

    title = "Directory Select"

    def __init__(self, root):
        FileDialog.__init__(self, root)

    #   Only allow selection of directories (single click)
    def files_select_event(self, e):
        pass
    #   Double click

    def files_double_event(self, e):
        pass

    #   Make sure it's a directory and accessable
    def ok_command(self):
        file = self.get_selection()
        if not os.path.isdir(file):
            if not os.access(file, R_OK):
                self.root.bell()
                file = None
        self.quit(file)

########################################


class MainWindow:
    def __init__(self, title=''):

        os.environ['TZ'] = 'GMT'
        time.tzname = ('GMT', 'GMT')
        time.timezone = 0

        self.root = Tk()
        Pmw.initialise(self.root)
        self.root.title(title)

        self.FileError = IntVar()
        self.InputError = IntVar()
        self.InfoString = StringVar()
        # self.Stream = IntVar()
        self.DataDirs = StringVar()
        self.RecordLength = IntVar()
        self.EventLength = IntVar()
        self.OutDir = StringVar()
        self.PCFFile = StringVar()
        self.Tolerance = IntVar()
        # self.Pad = IntVar()
        self.DASFile = StringVar()
        self.EVTFile = StringVar()
        self.EROUTFile = StringVar()
        self.SaveFile = StringVar()
        self.EqDir = StringVar()
        self.SampleRate = DoubleVar()
        self.SampleRateTolerance = DoubleVar()
        self.TraceDict = {}
        self.RateList = []

        self.createMainButtons(self.root)
        self.createNoteBooks(self.root)

        self.fileFilters = {
            "dasfile": "*das",
            "evtfile": "*evt",
            "pcffile": "*pcf"
        }

########################################

    def createMainButtons(self, master):
        """
        Build buttons for root window
        """
        self.InfoString_l = Label(master,
                                  bg="yellow",
                                  relief="ridge")
        self.InfoString_l.pack(side='bottom', fill='x')

        self.Button_fm = Frame(master, relief='groove', borderwidth=2)
        self.Button_fm.pack(side='bottom', pady=5, fill='x')

        Button(self.Button_fm,
               text="Exit",
               cursor='pirate',
               command=master.quit,
               activebackground='red',
               activeforeground='black').pack(side='right', anchor='e')

        Button(self.Button_fm,
               text="Cut Events",
               command=self.cutEvents,
               background='lightblue',
               activebackground='green',
               activeforeground='black').pack(side='right', anchor='e')

########################################

    def createNoteBooks(self, master):
        """
        Set up notebooks in root window
        """
        nb = Pmw.NoteBook(master)
        self.buildSetup(nb)
        self.buildDasFile(nb)
        self.buildEvtFile(nb)
        self.buildHelp(nb)
        nb.pack(padx=5, pady=5, fill='both', expand=1)

########################################

    def buildSetup(self, master):
        """
        Construct Setup NoteBook
        """
        self.Setup_nb = master.add('Setup')
        Label(self.Setup_nb,
              text='Required Input',
              bg='yellow',
              fg='black',
              relief='ridge',
              borderwidth=2).pack(side='top', anchor='n')

        # data selection frame
        self.DataDirs_fm = Frame(self.Setup_nb, relief='groove', borderwidth=2)
        self.DataDirs_fm.pack(side='top', pady=5, fill='x')

        Label(self.DataDirs_fm,
              text='Data Directories: '
              ).pack(side='left')

        Entry(self.DataDirs_fm,
              selectbackground='yellow',
              textvariable=self.DataDirs
              ).pack(side='left', anchor='w', expand=1, fill='x')

        Button(self.DataDirs_fm,
               activebackground='orange',
               relief="ridge",
               text="Clear",
               command=Command(self.setValue, self.DataDirs)
               ).pack(side='right')

        Button(self.DataDirs_fm,
               activebackground='green',
               relief="ridge",
               text="Find",
               command=Command(self.getPath, self.DataDirs, None)
               ).pack(side='right')
        self.DataDirs.set(os.getcwd())

        Button(self.DataDirs_fm,
               activebackground='green',
               relief="ridge",
               background='lightblue',
               text="Build Trace db",
               command=Command(self.BuildTrcList)
               ).pack(side='right')

########################################

    def FramesSetup(self):
        """
        Create Frames for Setup NoteBook
        These will be filled once a Trace db is created
        """
        self.SampleRate.set(0)
        try:
            self.SampleRate_fm.destroy()
        except Exception:
            pass
        self.SampleRate_fm = Frame(
            self.Setup_nb, relief='groove', borderwidth=2)
        self.SampleRate_fm.pack(side='top', pady=5, fill='x')

        try:
            self.RecordLength_fm.destroy()
        except Exception:
            pass
        self.RecordLength_fm = Frame(
            self.Setup_nb, relief='groove', borderwidth=2)
        self.RecordLength_fm.pack(side='top', pady=5, fill='x')

        try:
            self.EventLength_fm.destroy()
        except Exception:
            pass
        self.EventLength_fm = Frame(
            self.Setup_nb, relief='groove', borderwidth=2)
        self.EventLength_fm.pack(side='top', pady=5, fill='x')

        try:
            self.Opt_l.destroy()
        except Exception:
            pass
        self.Opt_l = Label(self.Setup_nb,
                           text='Optional Input',
                           bg='yellow',
                           fg='black',
                           relief='ridge',
                           borderwidth=2
                           )
        self.Opt_l.pack(side='top', anchor='n')

        try:
            self.OutDir_fm.destroy()
        except Exception:
            pass
        self.OutDir_fm = Frame(self.Setup_nb, relief='groove', borderwidth=2)
        self.OutDir_fm.pack(side='top', pady=5, fill='x')

        try:
            self.PCFFile_fm.destroy()
        except Exception:
            pass
        self.PCFFile_fm = Frame(self.Setup_nb, relief='groove', borderwidth=2)
        self.PCFFile_fm.pack(side='top', pady=5, fill='x')

        try:
            self.Tolerance_fm.destroy()
        except Exception:
            pass
        self.Tolerance_fm = Frame(
            self.Setup_nb, relief='groove', borderwidth=2)
        self.Tolerance_fm.pack(side='top', pady=5, fill='x')

        # try:
        #     self.Pad_fm.destroy()
        # except Exception:
        #     pass
        # self.Pad_fm = Frame(self.Setup_nb, relief = 'groove',
        #                     borderwidth = 2)
        # self.Pad_fm.pack(side = 'top', pady=5, fill = 'x')

########################################

    def FillSetup(self):
        """
        Populate Setup NoteBook
        """

        self.FramesSetup()
        Label(self.SampleRate_fm,
              text='Sample Rate of Input Data (sps): '
              ).pack(side='left')

        Pmw.ComboBox(self.SampleRate_fm,
                     history=1,
                     entry_width=10,
                     entry_textvariable=self.SampleRate,
                     scrolledlist_items=(self.RateList)
                     ).pack(side='left', anchor='w', expand=1)

        Label(self.SampleRate_fm,
              text='Sample Rate Tolerance (s): '
              ).pack(side='left')

        Entry(self.SampleRate_fm,
              width=10,
              selectbackground='yellow',
              textvariable=self.SampleRateTolerance
              ).pack(side='left', anchor='w', expand=1)

        Label(self.RecordLength_fm,
              text='Input Record Length (s): '
              ).pack(side='left')

        Entry(self.RecordLength_fm,
              width=10,
              selectbackground='yellow',
              textvariable=self.RecordLength
              ).pack(side='left', anchor='w', expand=1)

        Label(self.EventLength_fm,
              text='Output Record Length (s): '
              ).pack(side='left')

        Entry(self.EventLength_fm,
              width=10,
              selectbackground='yellow',
              textvariable=self.EventLength
              ).pack(side='left', anchor='w', expand=1)

        Label(self.OutDir_fm,
              text='Parent of Output Directory: '
              ).pack(side='left')

        Entry(self.OutDir_fm,
              selectbackground='yellow',
              textvariable=self.OutDir
              ).pack(side='left', anchor='w', expand=1, fill='x')

        Button(self.OutDir_fm,
               activebackground='red',
               text="Clear",
               command=Command(self.setValue, self.OutDir)
               ).pack(side='right')

        Button(self.OutDir_fm,
               activebackground='green',
               text="Find",
               command=Command(self.getPath, self.OutDir)
               ).pack(side='right')
        self.OutDir.set(os.getcwd())

        self.PCFFile_l = Label(self.PCFFile_fm,
                               text='PCF File: '
                               ).pack(side='left')

        Entry(self.PCFFile_fm,
              selectbackground='yellow',
              textvariable=self.PCFFile
              ).pack(side='left', anchor='w', expand=1, fill='x')

        Button(self.PCFFile_fm,
               activebackground='red',
               text="Clear",
               command=Command(self.setValue, self.PCFFile)
               ).pack(side='right')

        Button(self.PCFFile_fm,
               activebackground='green',
               text="Find",
               command=Command(self.findFile, self.PCFFile, None, "pcffile")
               ).pack(side='right')

        Label(self.Tolerance_fm,
              text='Tolerance (ms)'
              ).pack(side='left')

        Entry(self.Tolerance_fm,
              width=10,
              selectbackground='yellow',
              textvariable=self.Tolerance
              ).pack(side='left', anchor='w', expand=1)

        Label(self.Tolerance_fm, text="max tolerance when checking data "
              "continuity (0 => dft: 25% sample rate)").pack(side='left',
                                                             anchor='w',
                                                             expand=1)

        # Label(self.Pad_fm,
        #     text='Pre-Event Padding (s)'
        #     ).pack(side='left')
        #
        # Entry(self.Pad_fm,
        #     width = 10,
        #     selectbackground = 'yellow',
        #     textvariable = self.Pad
        #     ).pack(side='left', anchor='w',expand=1)
        #
        # Label(self.Pad_fm,
        #     text="new_event_time=event_time - pad"
        #     ).pack(side='left', anchor='w', expand=1)

########################################

    def buildDasFile(self, master):
        """
        Populate DAS File NoteBook
        """
        self.DASFile_nb = master.add('DAS File')
        self.DASFile_fm = Frame(
            self.DASFile_nb, relief='groove', borderwidth=2)
        self.DASFile_fm.pack(side='top', pady=5, fill='x')

        Label(self.DASFile_fm,
              text="DAS File: "
              ).pack(side='left')

        self.DASFile.set(os.getcwd() + "/" + "dasfile")
        Entry(self.DASFile_fm,
              selectbackground='yellow',
              textvariable=self.DASFile
              ).pack(side='left', anchor='w', expand=1, fill='x')
        self.DASFile.set(os.getcwd() + "/" + "dasfile")

        self.DASFileText = Pmw.ScrolledText(self.DASFile_nb,
                                            borderframe=1)

        Button(self.DASFile_fm,
               text='Create',
               activebackground='green',
               command=Command(self.MakeDasFile)
               ).pack(side='right', anchor='e')

        Button(self.DASFile_fm,
               text='load',
               activebackground='green',
               command=Command(self.updateTextFile,
                               self.DASFileText, self.DASFile)
               ).pack(side='right', anchor='e')

        Button(self.DASFile_fm,
               text='Save',
               activebackground='green',
               command=Command(self.saveFileWidget,
                               self.DASFile_nb, self.DASFile, "DAS")
               ).pack(side='right', anchor='e')

        Button(self.DASFile_fm,
               text='Find',
               activebackground='green',
               command=Command(self.findFile, self.DASFile,
                               self.DASFileText, "dasfile")
               ).pack(side='right')

        self.DASFileText.pack(side='bottom', fill='both', expand=1)

#######################################

    def buildEvtFile(self, master):
        """
        Populate Event File NoteBook
        """
        self.EVTFile_nb = master.add('Event File')
        self.EVTFile_fm = Frame(
            self.EVTFile_nb, relief='groove', borderwidth=2)
        self.EVTFile_fm.pack(side='top', pady=5, fill='x')

        Label(self.EVTFile_fm, text="Event File: ").pack(side='left')

        Entry(self.EVTFile_fm,
              selectbackground='yellow',
              textvariable=self.EVTFile
              ).pack(side='left', anchor='w', expand=1, fill='x')
        self.EVTFile.set(os.getcwd() + "/" + "evtfile")

        self.EVTFileText = Pmw.ScrolledText(self.EVTFile_nb,
                                            borderframe=1)

        # Button(self.EVTFile_fm,
        #     text='import',
        #     activebackground='green',
        #     command=Command(self.importCatalog, self.EVTFile,
        #                     self.EVTFileText)).pack(side='right', anchor='e')

        self.EVTFileText.pack(side='bottom', fill='both', expand=1)

        Button(self.EVTFile_fm,
               text='Save',
               activebackground='green',
               command=Command(self.saveFileWidget,
                               self.EVTFile_nb, self.EVTFile, "EVT")
               ).pack(side='right', anchor='e')

        Button(self.EVTFile_fm,
               text='load',
               activebackground='green',
               command=Command(self.updateTextFile,
                               self.EVTFileText, self.EVTFile)
               ).pack(side='right', anchor='e')

        Button(self.EVTFile_fm,
               text='Find',
               activebackground='green',
               command=Command(self.findFile, self.EVTFile,
                               self.EVTFileText, "evtfile")
               ).pack(side='right')

#######################################

    def buildCutEvent(self):
        """
        EqCut Status Log test window
        """
        try:
            self.tl_State = self.cutEventOutput_tl.winfo_exists()
        except Exception:
            self.tl_State = 0

        if self.tl_State == 1:
            self.cutEventOutput_tl.tkraise()
        else:
            # self.root.iconify()
            self.cutEventOutput_tl = Toplevel(self.root)
            self.cutEventOutput_tl.title("EqCut Status Log")

            self.Button_fm = Frame(self.cutEventOutput_tl,
                                   relief='groove',
                                   borderwidth=2)
            self.Button_fm.pack(side='bottom', pady=5, fill='x')

            self.Exit_b = Button(self.Button_fm,
                                 text="Exit",
                                 cursor='pirate',
                                 command=self.root.quit,
                                 activebackground='red',
                                 activeforeground='black')
            self.Exit_b.pack(side='right', anchor='e')

            self.ReCon_b = Button(self.Button_fm,
                                  activebackground='green',
                                  background='lightblue',
                                  text="Close",
                                  command=Command(self.CloseWindow,
                                                  self.cutEventOutput_tl,
                                                  self.root))
            self.ReCon_b.pack(side='right', anchor='e')

            # setup output file name
            (year, month, day, hour, minute, second, weekday, yearday,
             daylight) = time.localtime(time.time())
            now = ".".join(map(str, (year, yearday, hour, minute)))
            self.EROUTFile.set("tkeqcut_run." + now)

            self.Save_b = Button(self.Button_fm,
                                 text='Save Output',
                                 activebackground='green',
                                 command=Command(self.saveFileWidget,
                                                 self.cutEventOutput_tl,
                                                 self.EROUTFile, "EROUT"))
            self.Save_b.pack(side='right', anchor='e')

            self.EventCutText = Pmw.ScrolledText(self.cutEventOutput_tl,
                                                 borderframe=1)
            self.EventCutText.pack(side='bottom', fill='both', expand=1)

            # setup some tags for pretty formatting
            self.EventCutText.tag_config('blue', foreground='blue')
            self.EventCutText.tag_config('dkgreen', foreground='darkgreen')
            self.EventCutText.tag_config('red', foreground='red')
            self.EventCutText.tag_config('black', foreground='black')

#######################################

    def BuildTrcList(self):
        """
        build a trace list using DataDirList as base directories
        """
        self.addTextInfoBar(self.InfoString_l)

        DataDirList = []
        n = 0
        for idir in self.DataDirs.get().split(":"):
            dirlist = glob.glob(idir)
            for newdir in dirlist:
                if not os.path.isdir(newdir):
                    err = "***WARNING*** Directory " + newdir + " not found."
                    self.root.bell()
                    self.addTextInfoBar(self.InfoString_l, err, "red")
                    return
                DataDirList.append(newdir)

        (numSegy, numMseed, errfiles) = self.FindTrace(DataDirList)
        info = "Files Found: Segy = " + str(numSegy) + " ; mseed = " + \
            str(numMseed) + " ; Errors = " + str(errfiles)
        self.root.bell()
        self.addTextInfoBar(self.InfoString_l, info, "green")

########################################

    def FindTrace(self, DataDir):
        stack = DataDir
        # stack = []
        # for k in range(len(DataDir)) :
        #     stack.append(DataDir[k])
        self.RateList = []
        self.TraceDict = {}
        numSegy = numMseed = 0
        cnt = 1
        rwError = 0
        self.buildCutEvent()
        self.EventCutText.insert(
            "end",
            "\n\n##############New Trace Build ##############\n", "dkgreen")
        while stack:
            directory = stack.pop()
            if not os.path.isdir(directory):
                self.EventCutText.insert(
                    "end", "***WARNING*** Directory \"%s\" not found.\n" %
                    directory, "red")
                continue

            Ematch = re.search("EqCut", directory)
            if Ematch:
                self.EventCutText.insert(
                    "end", "WARNING: Will not search directory: \n\t%s\n" %
                    directory, "blue")
                continue

            self.EventCutText.insert(
                "end", "Searching directory:%s\n" % directory)
            for file in os.listdir(directory):
                if mod(cnt, 25):
                    pass
                else:
                    self.wait("Examining File: ", cnt)
                fullname = os.path.join(directory, file)

                if os.path.isfile(fullname):
                    # first attempt to id as a segy trace file
                    try:
                        newfile = Segy(fullname)
                        if newfile.isSegy():
                            (filetype, serial, chan, time, rate) = \
                                newfile.idhdr()
                            serial = str(serial)
                            chan = str(chan)
                            # build rate dictionary
                            if rate not in self.RateList:
                                self.RateList.append(rate)
                            epoch = self.ToEpoch(time)
                            # add to {sta{chan(rate,epoch,filetype,fullname)}}
                            # dictionary
                            if serial in self.TraceDict:
                                if chan in self.TraceDict[serial]:
                                    self.TraceDict[serial][chan].append(
                                        (rate, epoch, filetype, fullname))
                                else:
                                    self.TraceDict[serial].update(
                                        {chan: [(rate, epoch, filetype,
                                                 fullname)]})
                            else:
                                self.TraceDict.update(
                                    {serial: {chan: [(
                                        rate, epoch, filetype, fullname)]}})
                            numSegy += 1
                            newfile.close()
                            continue
                        else:
                            newfile.close()
                    except Exception:
                        rwError += 1
                        pass
                    else:
                        try:
                            # attempt to id as a mseed trace file
                            newfile = Mseed(fullname)
                            if newfile.isMseed():
                                try:
                                    # simple test to determine if correct
                                    # size file
                                    filesize = newfile.filesize
                                    blksize = newfile.blksize
                                    (numblocks, odd_size) = divmod(
                                        filesize, blksize)
                                    if odd_size:
                                        warn = "ERROR: File size is not an "\
                                            "integer number of block size ("\
                                            + str(blksize) + "). \n\t File: " \
                                            + fullname + "\n"
                                        self.EventCutText.insert(
                                            "end", warn, "red")
                                        rwError += 1
                                        continue
                                except Exception:
                                    err = "ERROR: Cannot determine file and "\
                                        "block sizes. \n\t File:" + fullname\
                                        + "\n"
                                    self.EventCutText.insert(
                                        "end", err, "red")
                                    rwError += 1
                                    continue

                                filetype = newfile.type
                                time = newfile.time
                                rate = newfile.rate
                                stat = newfile.FH.Stat.decode().strip()
                                chan = newfile.FH.Chan.decode().strip()
                                loc = newfile.FH.Loc.decode().strip()
                                net = newfile.FH.Net.decode().strip()
                                if loc:
                                    chan = chan + "_" + loc
                                # build rate dictionary
                                if rate not in self.RateList:
                                    self.RateList.append(rate)
                                epoch = self.ToEpoch(time)
                                # (epoch,eepoch)=newfile.FirstLastTime()
                                # add to
                                # {sta{chan(rate,epoch,filetype,fullname)}}
                                # dictionary
                                if stat in self.TraceDict:
                                    if chan in self.TraceDict[stat]:
                                        self.TraceDict[stat][chan].append(
                                            (rate, epoch, filetype, fullname))
                                    else:
                                        self.TraceDict[stat].update(
                                            {chan: [(rate, epoch, filetype,
                                                     fullname)]})
                                else:
                                    self.TraceDict.update(
                                        {stat: {chan: [(
                                            rate, epoch, filetype,
                                            fullname)]}})
                                numMseed += 1
                            newfile.close()
                        except Exception as e:
                            print("File Read Error: %s" % fullname, e)
                            rwError += 1
                            pass
                if os.path.isdir(fullname) or (os.path.islink(fullname) and
                                               not os.path.isfile(fullname)):
                    stack.append(fullname)
                cnt += 1
        self.RateList = sorted(self.RateList)
        self.FillSetup()
        return numSegy, numMseed, rwError

########################################

    def ToEpoch(self, trace_time):
        """
        Convert from yy:jjj.hh.mm.ss to epoch
        """
        trace_year = int(trace_time.split(":")[0])
        trace_day = int(trace_time.split(":")[1])
        trace_hour = int(trace_time.split(":")[2])
        trace_min = int(trace_time.split(":")[3])
        trace_sec = int(trace_time.split(":")[4])
        # convert from jday to day/month and determine epoch
        trace_year = int(Year2To4(trace_year))
        (mday, month) = CalDay(trace_day, trace_year)
        timestr = (trace_year, month, mday, trace_hour, trace_min,
                   trace_sec, 0, 0, -1)
        trace_epoch = time.mktime(timestr)
        return trace_epoch

########################################

    def MakeDasFile(self):
        """
        Build a DASFile based on the data discovered with self.FindTrace
        """
        self.DASFileText.clear()
        self.DASFileText.insert("end", "# das\tchan\n", 'header')
        n = 0
        for sta in self.TraceDict:
            prtlist = str(sta) + "\t"
            n = 0
            for chan in self.TraceDict[sta]:
                if not n:
                    prtlist = prtlist + str(chan)
                    n += 1
                else:
                    prtlist = prtlist + "," + str(chan)
            prtlist = prtlist + "\n"
            self.DASFileText.insert("end", prtlist, 'header')
            self.saveFileWidget(self.DASFile_nb, self.DASFile, 'DAS')

##########################################

    def killWindow(self, widget):
        """
        Destroy Widget and Set InfoBar to Default
        """
        widget.destroy()
        self.addTextInfoBar(self.InfoString_l)

##########################################

    def deiconWindow(self, widget):
        """
        deiconify Widget
        """
        widget.deiconify()

########################################

    def CloseWindow(self, master, icon):
        """
        Iconify Widget
        """
        self.addTextInfoBar(self.InfoString_l)
        master.destroy()
        icon.deiconify()

########################################

    def updateTextFile(self, var, file_entry):
        """
        Add File To Text Widget
        """
        var.clear()
        fileq = file_entry.get()
        if os.path.isfile(fileq):
            print(var)
            print(fileq)
            self.addTextInfoBar(
                self.InfoString_l, "Loading %s" % fileq, 'green')
            var.importfile(fileq)
        else:
            self.root.bell()
            self.addTextInfoBar(
                self.InfoString_l, "File %s does not exist" % fileq, 'red')

########################################

    def setValue(self, var, value=''):
        """
        Sets Value of var
        """
        var.set(value)

########################################

    def addTextInfoBar(self, widget, str='', bg='yellow'):
        """
        Adds Text To InfoBar
        """
        widget.configure(background=bg, text=str)

########################################

    def cutEvents(self):
        """
        Test input and file formats and then setup events to be cut
        """
        # check input
        self.root.update()
        self.checkInput()
        if not self.InputError.get():
            self.testFileFormat()
        if not self.FileError.get() and not self.InputError.get():
            if self.createOutDir():
                return  # bail if we can't create an output directory

            # build/raise EqCut Status Log
            self.buildCutEvent()

            # parse event and das files to create usable lists
            eventDict = self.ParseFile(self.EVTFile, 'evtnum', 'evttime')
            daschanDict = self.ParseFile(self.DASFile, 'das', 'chan')

            # loop over events
            for evtnum in eventDict:
                event_time = self.CorrectEvent(eventDict[evtnum])
                self.EventCutText.insert(
                    "end", "\n\n**********************************\n",
                    "dkgreen")
                self.EventCutText.insert(
                    "end", "* Cutting Event: %s *\n" % eventDict[evtnum],
                    "dkgreen")
                self.EventCutText.insert(
                    "end", "**********************************\n", "dkgreen")

                evt_epoch = self.ToEpoch(eventDict[evtnum])
                min_epoch = evt_epoch - float(self.RecordLength.get())
                max_epoch = evt_epoch + float(self.EventLength.get())

            # create output dir
                EvtDir = "E" + eventDict[evtnum]
                self.EqDir.set("")
                eqdir = self.OutDir.get() + "/" + "EqCut" + "/" + EvtDir + \
                    "_" + str(self.SampleRate.get())
                if not os.path.exists(eqdir):
                    try:
                        os.makedirs(eqdir)
                    except OSError as e:
                        print("Can't mkdir %s" % (eqdir), e)
                        return
                self.EqDir.set(eqdir)
                self.EventCutText.insert(
                    "end", "\nStoring merged data in:\n%s\n" % eqdir)

                # loop over das and chan testing for sample rate and
                # epoch time matches
                for das in daschanDict:
                    if das in self.TraceDict:
                        channels = []
                        chan_list = daschanDict[das].split(",")
                        for chan in chan_list:
                            self.addTextInfoBar(
                                self.InfoString_l,
                                "Cutting Event: %s\tStat: %s\tChan: %s" %
                                (eventDict[evtnum], das, chan), 'lightblue')
                            file_list = {}
                            if chan in self.TraceDict[das]:
                                for tup in\
                                        range(len(self.TraceDict[das][chan])):
                                    irate = self.TraceDict[das][chan][tup][0]
                                    iepoch = self.TraceDict[das][chan][tup][1]
                                    filetype = \
                                        self.TraceDict[das][chan][tup][2]
                                    if not self.testRate(irate):
                                        continue
                                    if not self.testEpoch(
                                       iepoch, min_epoch, max_epoch):
                                        continue
                                    if filetype in file_list:
                                        file_list[filetype].append(
                                            self.TraceDict[das][chan][tup][3])
                                    else:
                                        file_list.update({
                                            filetype: [self.TraceDict[das][
                                                chan][tup][3]]})

                            # now setup program args
                            self.RunProg(file_list, event_time, das, chan)

                # since we set up the EqDir before running segymerge or
                # qmerge, not test to see if anything got written.
                # If not, remove EqDir
                if self.EqDir.get():
                    globfiles = self.EqDir.get() + "/*"
                    eqfiles = glob.glob(globfiles)
                    if not eqfiles:
                        self.EventCutText.insert(
                            "end", "\nEvent %s had no data to merge!\n" %
                            eventDict[evtnum], "red")
                        self.EventCutText.insert(
                            "end", "%s is empty.\n" % globfiles, "red")
                        self.EventCutText.insert(
                            "end", "Removing %s\n" % globfiles, "red")
                        try:
                            os.rmdir(self.EqDir.get())
                        except OSError as e:
                            self.EventCutText.insert(
                                "end", "Can't remove %s\n" % e, "red")

            self.addTextInfoBar(self.InfoString_l, "Done", 'green')

########################################################

    def RunProg(self, filedict, evt, das, chan):
        """
        create command lines args for segymerge and qmerge, and run
        """

        self.TestPasscal()

        for filetype in filedict:
            file_list_string = ""
            other_flags_string = ""
            for k in range(len(filedict[filetype])):
                file_list_string = file_list_string + " " + \
                    filedict[filetype][k]
            self.EventCutText.insert(
                "end", "\n---Converting event: %s\n" % evt, "blue")
            self.EventCutText.insert(
                "end", "---Das: %s Chan: %s\n" % (das, chan), "blue")
            self.EventCutText.insert(
                "end", "---Sample Rate: %d\n" % self.SampleRate.get(), "blue")
            self.EventCutText.insert(
                "end", "---Traces to merge: %i\n" % len(filedict[filetype]),
                "blue")
            self.EventCutText.insert(
                "end", "---Trace Type: %s\n\n" % filetype, "blue")

            if filetype == "mseed":
                filename = ".".join((evt, das, chan, "m"))
                (year, day, hour, min, sec) = evt.split(":")
                year = Year2To4(int(year))
                evt = ".".join((year, day, hour)) + ":" + ":".join((min, sec))
                if self.PCFFile.get():
                    self.addTextInfoBar(
                        self.InfoString_l,
                        "***WARNING***: You must time correct mseed data "
                        "before merging", "orange")
                if self.Tolerance.get():
                    other_flags_string = other_flags_string + " " + \
                        "-g" + " " + str(self.Tolerance.get())
                comstr = self.MseedProg + " -f " + evt + " -s " + \
                    str(self.EventLength.get()) + \
                    " -T " + other_flags_string + file_list_string + \
                    " > " + self.EqDir.get() + "/" + filename
                self.EventCutText.insert(
                    "end", "\nCommand Issued: %s\n" % comstr)
            else:
                filename = ".".join((evt, das, chan))
                if self.PCFFile.get():
                    other_flags_string = other_flags_string + " " + \
                        "-r " + self.PCFFile.get()
                if self.Tolerance.get():
                    other_flags_string = other_flags_string + " " + \
                        "-t" + " " + str(self.Tolerance.get())
                comstr = self.SegyProg + " -c " + file_list_string + " -s " + \
                    evt + " -l " + str(self.EventLength.get()) + " " + \
                    other_flags_string + " > " + self.EqDir.get() + "/" + \
                    filename
                self.EventCutText.insert(
                    "end", "\nCommand Issued: %s\n" % comstr)

            self.EventCutText.update()
            # runprog = popen2.Popen3(comstr)
            runprog = Popen(comstr, shell=True, stdin=PIPE,
                            stdout=PIPE, stderr=PIPE, close_fds=True)
            while runprog.poll() == -1:
                continue
            else:
                return

########################################################

    def ParseFile(self, argfile, k, v):
        """
        parse input file
        using input_order assign variables to input_list hash
        skip any line beginning with #
        use input_order as keys to input_list
        initiate input_list
        """
        infile = open(argfile.get(), 'r')
        eline = infile.readline()

        input_order = eline[1:].split()

        input_dict = {}  # initiate return dictionary input_dict
        return_dict = {}  # initiate return dictionary
        while True:
            newline = infile.readline()
            if not newline:
                break
            if newline[0] == "#":
                continue
            if newline[:-1] == "":
                continue
            newline = newline.split()
            i = 0
            for key in input_order:
                input_dict[key] = newline[i]
                i += 1
            if "evtnum" in input_order:
                return_dict[input_dict[k]] = input_dict[v]
            else:
                try:
                    return_dict[input_dict[k]] = \
                        return_dict[input_dict[k]] + "," + input_dict[v]
                except Exception:
                    return_dict[input_dict[k]] = input_dict[v]
        infile.close()
        return return_dict

#######################################

    def CorrectEvent(self, evttime):
        """
        converts a 4 digit event year to a 2 digit event year
        """

        (year, day, hour, min, sec) = evttime.split(":")
        if len(year) == 4:
            year = Year4To2(int(year))
        return ":".join((year, day, hour, min, sec))

#######################################

    def testRate(self, rate):
        """
        test if sample rate is within search boundaries
        """
        float(rate)
        min = float(self.SampleRate.get() - self.SampleRateTolerance.get())
        max = float(self.SampleRate.get() + self.SampleRateTolerance.get())
        if min <= rate and rate <= max:
            return 1
        else:
            return 0

#######################################

    def StripSpaces(self, var):
        """
        strips blanks from string
        """
        newvar = ""
        for i in range(len(var)):
            if var[i] != " ":
                newvar = newvar + var[i]
        return newvar

#######################################

    def testEpoch(self, epoch, min, max):
        """
        test if epoch time is within search boundaries
        """
        if min <= epoch and epoch <= max:
            return 1
        else:
            return 0

#######################################

    def wait(self, words, cnt):
        """
        routine to put words on info bar, used to count traces
        being examined
        """

        txt = words + str(cnt)
        self.root.update()
        self.addTextInfoBar(self.InfoString_l, txt, 'lightblue')

#############################################

    def createOutDir(self):
        """
        Test if output directory exists, if not create else return error
        """
        err = None
        if not os.path.isdir(self.OutDir.get()):
            try:
                print(self.OutDir.get())
                os.makedirs(self.OutDir.get(), 0o755)
            except OSError as e:
                err = "Can't Create %s" % self.OutDir.get(), e
                self.addTextInfoBar(self.InfoString_l, err, 'red')
        return err
# END createOutDir

#############################################

    def testFileFormat(self):
        """
        Test if files have first line defining input order
        """
        self.FileError.set(1)

        try:
            dasfile = open(self.DASFile.get(), "r")
        except IOError as e:
            err = "Can't open file %s" % self.DASFile.get(), e
            self.addTextInfoBar(self.InfoString_l, err, 'red')
            self.root.bell()
            return err
        dline = dasfile.readline()
        input_order = dline[1:].split()
        if dline[0] != "#" or "das" not in input_order \
           or "chan" not in input_order:
            err = "First line of DAS file %s must begin with # and define "\
                "input order" % self.DASFile.get()
            dasfile.close()
            self.addTextInfoBar(self.InfoString_l, err, 'red')
            self.helpFileFormat(self.root)
            self.root.bell()
            return err

        try:
            evtfile = open(self.EVTFile.get(), "r")
        except IOError as e:
            err = "Can't open file %s" % self.EVTFile.get(), e
            self.addTextInfoBar(self.InfoString_l, err, 'red')
            self.root.bell()
            return err
        eline = evtfile.readline()
        input_order = eline[1:].split()
        if eline[0] != "#" or "evtnum" not in input_order \
           or "evttime" not in input_order:
            err = "First line of EVT file %s must begin with # "\
                "and define input order" % self.EVTFile.get()
            evtfile.close()
            self.addTextInfoBar(self.InfoString_l, err, 'red')
            self.EVTFileText.clear()
            self.EVTFileText.insert(
                "end", "<HEADER FIELDS MISSING OR INCOMPLETE>\n", 'header')
            self.EVTFileText.tag_config('header', foreground='red')
            self.EVTFileText.importfile(self.EVTFile.get())
            self.helpFileFormat(self.root)
            self.root.bell()
            return err

        dasfile.close()
        evtfile.close()
        self.FileError.set(0)

# END testFileFormat

    def TestPasscal(self):
        self.SegyProg = "segymerge"
        self.MseedProg = "qmerge"

#############################################

    def saveFileWidget(self, master, file="", filetype=""):
        """
        Toplevel to save file and check format
        """

        try:
            self.tl_State = self.saveFileWidget_tl.winfo_exists()
        except Exception:
            self.tl_State = 0

        if self.tl_State == 1:
            # self.addTextInfoBar(self.InfoString_l, "Save As Already Open",
            #   'orange')
            self.saveFileWidget_tl.tkraise()
        else:
            self.FileType = filetype
            self.SaveFile.set(file.get())
            box_length = len(self.SaveFile.get()) + 15
            self.saveFileWidget_tl = Toplevel(master)
            self.saveFileWidget_tl.title("Save %s File As:" % filetype)
            self.saveFileWidgetCancel_b = Button(
                self.saveFileWidget_tl,
                text="Cancel",
                cursor='pirate',
                activebackground='red',
                command=Command(self.killWindow, self.saveFileWidget_tl)
            )
            self.saveFileWidgetCancel_b.pack(side='bottom', fill='x')

            self.saveFileWidgetSave_b = Button(
                self.saveFileWidget_tl,
                text="Save",
                background='lightblue',
                activebackground='green',
                command=Command(
                    self.writeFile, self.saveFileWidget_tl,
                    self.SaveFile, self.FileType)
            )
            self.saveFileWidgetSave_b.pack(side='bottom', fill='x')

            self.saveFileWidgetEntry_fm = Frame(
                self.saveFileWidget_tl,
                relief='groove',
                borderwidth=2
            )
            self.saveFileWidgetEntry_fm.pack(side='left',
                                             fill='x',
                                             pady=5
                                             )

            self.saveFileWidgetEntry_e = Entry(
                self.saveFileWidgetEntry_fm,
                textvariable=self.SaveFile,
                width=box_length
            )
            self.saveFileWidgetEntry_e.pack(
                side='left', anchor='w', fill='x', expand=1)

##########################################

    def getPath(self, var, clear=1):
        """
        Concatonate paths
        """
        self.var = var
        if clear:
            self.var.set('')
        newpath_dd = DirectoryDialog(self.root)
        self.path = newpath_dd.go(dir_or_file=os.getcwd())
        if self.path is not None:
            if self.var.get():
                self.var.set(self.var.get() + ':')
            if len(self.path) == 1 and self.path[0] == "/":
                self.var.set(self.var.get() + self.path)
            elif self.path[-1] == "/":
                self.var.set(self.var.get() + self.path[:-1])
            else:
                self.var.set(self.var.get() + self.path)

##########################################

    def findFile(self, file, widget, filetype):
        """
        Find a file and import into text window
        """
        self.filetype = filetype
        newfile = askopenfilename(
            filetypes=[("All", "*"),
                       (self.filetype, self.fileFilters[self.filetype])])
        if newfile:
            file.set(newfile)
            if widget:
                widget.clear()
                widget.importfile(file.get())

##########################################

    def importCatalog(self, file, widget):
        """
        Find a file and import into text window
        """
        info = "This function is not available."
        self.addTextInfoBar(self.InfoString_l, info, 'orange')
        return
        # newfile = tkFileDialog.askopenfilename\
        #     (filetypes=[("All", "*"), ("HDF", "*hdf"), ("EHDF", "*ehdf")])
        # if newfile:
        #     file.set(newfile)
        #     if widget:
        #         widget.clear()
        #         widget.importfile(file.get())

##########################################

    def writeFile(self, master, file, filetype):
        """
        Write File to disk
        """

        if os.path.isfile(file.get()):
            self.Continue_dl = Pmw.MessageDialog(
                master,
                title="WARNING",
                defaultbutton=1,
                buttons=('Overwrite', 'Cancel'),
                message_text='The File exists!'
            )
            self.Result = self.Continue_dl.activate()
        else:
            self.Result = "Overwrite"

        if (self.Result == "Cancel"):
            self.addTextInfoBar(self.InfoString_l)
        elif (self.Result == "Overwrite"):
            master.destroy()
            if (filetype == "DAS"):
                if len(self.DASFileText.get()) == 1:
                    err = "DASFileText is empty, nothing to save!"
                    self.addTextInfoBar(self.InfoString_l, err, 'orange')
                else:
                    try:
                        self.DASFileText.exportfile(file.get())
                        self.addTextInfoBar(
                            self.InfoString_l,
                            "Wrote DAS File: %s " % file.get(), 'green')
                        self.DASFile.set(file.get())
                    except IOError as e:
                        err = "Can't Create %s" % file.get(), e
                        self.addTextInfoBar(self.InfoString_l, err, 'red')
            elif (filetype == "EVT"):
                if len(self.EVTFileText.get()) == 1:
                    err = "EVTFileText is empty, nothing to save!"
                    self.addTextInfoBar(self.InfoString_l, err, 'orange')
                else:
                    try:
                        self.EVTFileText.exportfile(file.get())
                        self.addTextInfoBar(
                            self.InfoString_l,
                            "Wrote EVT File: %s " % file.get(), 'green')
                        self.EVTFile.set(file.get())
                    except IOError as e:
                        err = "Can't Create %s" % file.get(), e
                        self.addTextInfoBar(self.InfoString_l, err, 'red')
            elif (filetype == "EROUT"):
                if len(self.EventCutText.get()) == 1:
                    err = "EventCutText is empty, nothing to save!"
                    self.addTextInfoBar(self.InfoString_l, err, 'orange')
                else:
                    try:
                        self.EventCutText.exportfile(file.get())
                        self.addTextInfoBar(
                            self.InfoString_l,
                            "Wrote OutPut File: %s " % file.get(), 'green')
                    except IOError as e:
                        err = "Can't Create %s" % file.get(), e
                        self.addTextInfoBar(self.InfoString_l, err, 'red')

###############################################

    def checkInput(self):
        """
        Test if input is valid and complete
        """

        self.InputError.set(1)
        try:
            self.EventLength.get()
        except Exception:
            self.root.bell()
            err = "ERROR: No Output Record Length given"
            self.addTextInfoBar(self.InfoString_l, err, 'red')
            return

        try:
            self.RecordLength.get()
        except Exception:
            self.root.bell()
            err = "ERROR: No Input Record Length given"
            self.addTextInfoBar(self.InfoString_l, err, 'red')
            return

        try:
            self.Tolerance.get()
        except Exception:
            self.root.bell()
            err = "ERROR: Tolerance must be integer or 0 "\
                "(0 => 25% sample rate)"
            self.addTextInfoBar(self.InfoString_l, err, 'red')
            return

        # try:
        #     self.Pad.get()
        # except Exception:
        #     self.root.bell()
        #     err="ERROR: Pad must be integer or 0"
        #     self.addTextInfoBar(self.InfoString_l, err, 'red')
        #     return

        if not self.DataDirs.get():
            err = "ERROR: No DataDirs defined"
        elif not self.DASFile.get():
            err = "ERROR: No DAS File given"
        elif not os.path.isfile(self.DASFile.get()):
            err = "ERROR: DAS File %s doesn't exist" % self.DASFile.get()
        elif not self.EVTFile.get():
            err = "ERROR: No Event File given"
        elif not os.path.isfile(self.EVTFile.get()):
            err = "ERROR: Event File %s doesn't exist" % self.EVTFile.get()
        elif not self.EventLength.get():
            err = "ERROR: No Output Record Length given"
        # elif not self.RecordLength.get():
        #     err="ERROR: No Input Record Length given"
        elif not self.SampleRate.get():
            err = "ERROR: No Input Sample Rate given"
        else:
            self.InputError.set(0)
            self.addTextInfoBar(self.InfoString_l)
        if self.InputError.get():
            self.root.bell()
            self.addTextInfoBar(self.InfoString_l, err, 'red')

########################################

    def buildHelp(self, master):
        """
        Populate Help NoteBook
        """
        self.Help_nb = master.add('Help')

        self.helpFileFormat_b = Button(self.Help_nb,
                                       activebackground='green',
                                       background='lightblue',
                                       text="File Formats",
                                       activeforeground='black',
                                       command=Command(
                                           self.helpFileFormat, self.Help_nb)
                                       )
        self.helpFileFormat_b.pack(side='top', anchor='e', padx=5, pady=5)

        self.HelpText = Pmw.ScrolledText(self.Help_nb,
                                         borderframe=1)
        self.HelpText.pack(side='bottom', fill='both', expand=1)

        self.HelpText.settext("""
NAME
    tkeqcut - GUI to configure input for segymerge and qmerge

VERSION
    %s

SYNOPSIS
    tkeqcut is a front-end GUI for segymerge and qmerge

DESCRIPTION
    tkeqcut has four Notebook windows: Setup, Das_File, Event_File, and help.
    These windows allow you to enter necessary information for tkeqcut to pass
    to segymerge and qmerge.

  Setup Window

    The Setup window has both required and optional parameters.

    Required parameters are:

        Data Directories - parent direcory(ies) of segy and mseed data.
            Multiple locations are separated by colons (:)

        Sample Rate of Input Data (sps) - sample rate of input data records

        Sample Rate Tolerance (s) - +- tolerance used when searching for data
            (this is useful when accessing Antelope data)

        Input Record Length (s) - length of input data records needed for
            data search

        Event Record Length (s) - length of output data records

    Optional parameters are:

        Output Directory (default:./) -

            Location for saving output events. Events are stored in
                directories labelled: EYYYY.DDD.HH.MM.SS_sps where
                YYYY.DDD.HH.MM.SS is the event time and sps is the samples
                per second of input trace(s).

        PCF File -

            This file is passed to segymerge or qmerge which performs a
                clock correction via the same algorithm as clockcor on the
                data before any mergeing or cutting.

        Tolerance -

            Specifies the maximum tolerance in milliseconds the program
                should take into account when checking the continuity of the
                data.

  Das_File Notebook

    This notebook allows you to specify, enter, and edit das files.

    The four buttons are:

        Find - selects and loads file into text window

        Save - pop up window to save file as

        Load - loads file into text window for editting

        Create - creates a das file based on the trace data found.


  Event_File Notebook

    This notebook allows you to specify, enter, and edit event files.

    The Three buttons are:

        Find - selects and loads file into text window

        Save - pop up window to save file as

        Load - loads file into text window for editting

            NOTES:

            Once a file is entered in the "Das/Event File:" entry box it is
            active (e.g. loading is not required for the code to recognize
            the entry).

            Changes made to a loaded file do not take affect until saved.

KEYWORDS
    tkeqcut, segymerge, qmerge

SEE ALSO
    the segymerge manpage the qmerge manpage

AUTHOR
    Bruce Beaudoin <bruce@passcal.nmt.edu>
""" % VERSION)

########################################

    def helpFileFormat(self, master):
        """
        Provide DAS and Evt file format help and examples
        """

        try:
            self.tl_State = self.helpFileFormat_tl.winfo_exists()
        except Exception:
            self.tl_State = 0

        if self.tl_State == 1:
            self.addTextInfoBar(
                self.InfoString_l, "File Formats Already Open", 'orange')
            self.helpFileFormat_tl.tkraise()
        else:
            self.helpFileFormat_tl = Toplevel(master)
            self.helpFileFormat_tl.title("DAS and Evt File Formats")

            self.ExitFileHelp_b = Button(self.helpFileFormat_tl,
                                         activebackground='red',
                                         cursor='pirate',
                                         background='lightblue',
                                         text="Done",
                                         command=Command(
                                             self.killWindow,
                                             self.helpFileFormat_tl))
            self.ExitFileHelp_b.pack(side='bottom', fill='x', expand=1)
            self.FileFormatText = Pmw.ScrolledText(self.helpFileFormat_tl,
                                                   borderframe=1)
            self.FileFormatText.pack(side='bottom', fill='both', expand=1)
            self.FileFormatText.settext("""
das_file 1st line should define columns and begin with #
       You can put anything in this file providing it contains the
       two key columns of das and chan.
       (e.g. # das site profile lat lon elv geophone chan)
       The easiest way to produce this list is by using the
       DAS_File->Create button.

               das : station name/serial number
               chan_loc: channels recorded (comma separated list)
                   If location codes exist delineate from channel
                   with an underscore (_).
                     NOTE: for mseed files produced from ref2mseed
                           channels will be in the form - streamCchannel
                           (e.g. 1C4 for data stream 1 channel 4)

==============================================================

event_file 1st line should define columns and begin with #
       You can put anything in this file providing it contains the
       two key columns of evtnum and evttime.
       (e.g. # evtnum shotnum starttime evttime lat lon elv depth size)

               evtnum : sequential shot number for experiment
               evttime : event time (yy:ddd:hh:mm:ss)

EXAMPLE dasfile:

# das chan
0152 4,5,6
7066 1,2,3
6021 1C4,1C5,1C6
0109 BHN,BHE,BHZ
# with location codes
7903 BHN_01,BHE_01,BHZ_01,BHN_02,BHE_02,BHZ_02


EXAMPLE evtfile:

# evtnum evttime
101    99:365:03:00:00
102    00:216:17:05:33


NOTES:
Columns can be in any order.
Unrecognized column headings will be ignored.
Lines beginning with # will be ignored (except header line).
Blank lines will be ignored.
""")

########################################


def main():
    # return version number if -# command line option
    if len(sys.argv) == 1:
        print(os.path.basename(sys.argv[0]), VERSION)
    elif len(sys.argv) == 2 and sys.argv[1] == "-#":
        print(VERSION)
        sys.exit(1)
    mw = MainWindow("EqCut %s" % VERSION)
    mw.root.geometry("670x570")
    mw.root.mainloop()


if __name__ == '__main__':
    main()
