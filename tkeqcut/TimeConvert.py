#!/usr/bin/env python

####################
#
# version: 2007.024
# modification
#
# author: bcb
#
# cleaned up code
#
################################################################
#
# modification
# version: 2019.028
# author: Lan Dam
#
# convert to be compatible with both py2 and py3

import sys
import time
from os import environ
from sys import exit

VERSION = "2022.1.0.0"

environ['TZ'] = 'GMT'
time.tzname = ('GMT', 'GMT')
time.timezone = 0


###############################################

def AddLeadZero(hms):
    """
    fcn to put zero in front of 0-9,  returns time as string
    """
    if hms < 10:
        return '0' + str(hms)
    else:
        return str(hms)

###############################################


def AddLeadZeroJday(jday):
    """
    sub to put zeros in front of jday, returns jday as string
    """
    if jday < 10:
        return '00' + str(jday)
    elif jday < 100:
        return '0' + str(jday)
    else:
        return str(jday)

###############################################


def Year4To2(y4):
    """
    convert from 4 digit to 2 digit year with floor of 1970, returns y4 as
    string
    """
    y4 = int(y4)
    if y4 >= 1970:
        y4 -= 1900
    else:
        exit("ERROR: Code does't handle dates prior to 1970")
    if y4 >= 100:
        y4 -= 100
        y4 = AddLeadZero(y4)
    return str(y4)

###############################################


def Year2To4(y2):
    """
    convert from 2 digit year (since 1970) to 4 digit year, returns y2 as
    string
    """
    y2 = int(y2)
    if y2 >= 70 and y2 < 100:
        y2 += 1900
    elif y2 >= 0 and y2 < 70:
        y2 += 2000
    return str(y2)

###############################################


def CalDay(jday, year):
    """
    sub to convert from julian day to calendar day
    days in calendar year [0]=normal [1]=leap
    """
    days_in_month = (
        [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    )

    # determine if leap year
    if year % 4 == 0 and year % 100 != 0 or year % 400 == 0:
        leap = 1
    else:
        leap = 0

    if jday == 366 and leap == 0:
        return ("ERROR: Year", year, "Does Not Have 366 Days")
    else:
        i = 1
        mon = 0
        mday = - 1
        while jday > days_in_month[leap][i]:
            jday -= days_in_month[leap][i]
            mon = i
            mday = jday
            i += 1

        mon += 1
        if mday < 0:
            mday = jday
        return mday, mon

###########################################


def Epoch2EvtTime(epoch):
    """
    sub to determine epoch and search times
    return to jday times for max time Note 1900=00
    """
    (year, month, day, hour, minute, second, weekday, yday, dst) = \
        time.gmtime(epoch)

    # if not dst : hour-=1
    # fix with zeros
    second = AddLeadZero(second)
    minute = AddLeadZero(minute)
    hour = AddLeadZero(hour)
    yday = AddLeadZeroJday(yday)

    # calc yyyydddhhmmss for get_trace
    no_dot_time = "".join(map(str, (year, yday, hour, minute, second)))

    # return year back to 2 digits
    year = Year4To2(int(year))
    year = AddLeadZero(year)

    dot_time = ".".join(map(str, (year, yday, hour, minute, second)))
    return no_dot_time, dot_time


################################
# Test
################################

if __name__ == "__main__":

    if len(sys.argv) > 1:
        if sys.argv[1] == "-#":
            print(VERSION)
            sys.exit(1)
        else:
            print("Unknown arguement %s" % sys.argv[1:])
            sys.exit(1)

    print("\n***Testing Functions***")

    print("\nTesting AddLeadZero")
    for x in range(5, 20, 10):
        print("\tInput ", x, " : Output", AddLeadZero(x))

    print("\nTesting AddLeadZeroJday")
    for x in range(5, 150, 50):
        print("\tInput ", x, " : Output", AddLeadZeroJday(x))

    print("\nTesting Year4To2")
    for x in range(1970, 2050, 35):
        print("\tInput ", x, " : Output", Year4To2(x))

    print("\nTesting Year2To4")
    for x in range(0, 100, 35):
        print("\tInput ", x, " : Output", Year2To4(x))
    print("\tInput 04 : Output", Year2To4(0o4))
    print("\tInput 96 : Output", Year2To4(96))
    print("\nTesting CalDay")
    for x in (1, 45, 100, 365, 366):
        for y in (1970, 2000, 2010, 2016):
            print("\tInput ", x, ", ", y, " : Output", CalDay(x, y))

    print("\nTesting Epoch2EvtTime")
    test_time = time.time()
    print("\tTimeZone: ", time.timezone)
    print("\tDate: ", time.ctime(test_time))
    print("\tUTC: ", time.gmtime(test_time))
    print("\tInput", test_time, ": Output", Epoch2EvtTime(test_time))
    test_string = (1996, 6, 11, 3, 28, 0, 0, 0, -1)
    print(test_string)
    test_epoch = time.mktime(test_string)
    print(test_epoch)
    print(time.gmtime(test_epoch))
    print(Epoch2EvtTime(test_epoch))
