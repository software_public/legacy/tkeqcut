# -*- coding: utf-8 -*-

"""Top-level package for tkeqcut."""

__author__ = """IRIS PASSCAL"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2022.1.0.0'
