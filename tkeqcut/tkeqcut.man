.TH tkeqcut 2007.024 "PASSCAL MANUAL PAGES"
.SH NAME
tkeqcut \- GUI to configure input for segymerge and qmerge
.SH SYNOPSIS
\fBtkeqcut\fR  [\fB-#\fR]
.LP
.SH OPTIONS
.TP
\fB-#\fR returns version number
.SH DESCRIPTION
\fBtkeqcut\fR
is a front-end GUI for \fBsegymerge\fR  and \fBqmerge\fR which are used for cutting
earthquakes/events from continuous data.
.LP
\fBtkeqcut\fR has four notebook windows: Setup, Das_File, Event_File, and Help.
These windows allow you to enter necessary information for \fBtkeqcut\fR to pass
to \fBsegymerge\fR  and \fBqmerge\fR.
.LP
.SH Setup Window
.LP
The Setup window has both required and optional parameters.
.LP
\fBRequired parameters are:\fR

.TP
Data Directories (default:./) -
Parent direcory(ies) of segy and mseed data. 
Multiple locations are separated by colons (:).
.TP
Sample Rate of Input Data (sps) - 
sample rate of input data records.
.TP
Sample Rate Tolerance (s) - 
+-tolerance used when searching for data
(this is useful when accessing Antelope data)
.TP
Input Record Length (s) - 
length of input data records needed for data search
.TP
Event Record Length (s) -
length of output data records
.TP
\fBOptional parameters are:\fR
.TP
Output Directory (default:./) -
Location for saving output events. Events are stored in
directories labelled: EYYYY.DDD.HH.MM.SS_sps where YYYY.DDD.HH.MM.SS
is the event time and sps is the samples per second of input trace(s).
.TP
PCF File -
This file is passed to segymerge or qmerge which performs a
clock correction via the same algorithm as clockcor on the data
before any mergeing or cutting.
.TP
Tolerance -
Specifies the maximum tolerance in milliseconds the program
should take into account when checking the continuity of the
data.
.LP
.SH Das_File Notebook
.LP
This notebook allows you to specify, enter, and edit das files.
.LP
Four buttons allow you to:
.LP
\fBFind\fR - selects and loads file into text window
.LP
\fBSave\fR - pop up window to save file as
.LP
\fBLoad\fR - loads file into text window for editting.
.LP
\fBCreate\fR - creates a das file based on the trace data found.
.LP
.SH Event_File Notebook
.LP
This notebook allows you to specify, enter, and edit event files.
.LP
Three buttons allow you to:
.LP
\fBFind\fR - selects and loads file into text window
.LP
\fBSave\fR - pop up window to save file as
.LP
\fBLoad\fR - loads file into text window for editting
.TP
\fBDas_File and Event_File Window Notes:\fR
Once a file is entered in the "Das/Event File:" entry box it is
active (e.g. loading is not required for the code to recognize
the entry).
Changes made to a loaded file do not take affect until saved.

.SH FILE FORMATS
.TP
\fBdas_file:\fR
1st line  defines columns and begin with #
You can put anything in this file providing it contains the
two key columns of \fBdas\fR and \fBchan\fR.
(e.g. # das site profile lat lon elv geophone chan)

.nf
\fIdas\fR - station name/serial number 
\fIchan_loc\fR - channels recorded (comma separated list)
If location codes exist delineate from channel
with an underscore (_).
NOTE: for mseed files produced from ref2mseed
channels will be in the form - streamCchannel
(e.g. 1C4 for data stream 1 channel 4)
\fBThe easiest way to produce this list is by using the 
DAS_File->Create button.\fR

\fBExample\fR
# das chan
0152 4,5,6
7066 1,2,3
6021 1C4,1C5,1C6
0109 BHN,BHE,BHZ
# with location codes
7903 BHN_01,BHE_01,BHZ_01,BHN_02,BHE_02,BHZ_02
.fi
.TP
\fBevent_file\fR
1st line should define columns and begin with #
You can put anything in this file providing it contains the
two key columns of \fBevtnum\fR and \fBevttime\fR.
(e.g. # evtnum shotnum starttime evttime lat lon elv depth size)

.nf
\fIevtnum\fR - sequential shot number
\fIevttime\fR - event time (yy:ddd:hh:mm:ss)

\fBExample\fR
# evtnum evttime
101	99:365:03:00:00
102	00:216:17:05:33
.fi
.TP
\fBFile Format Notes:\fR
Columns can be in any order.
Unrecognized column headings will be ignored.
Lines beginning with # will be ignored (except header line).
Blank lines will be ignored.

.SH KEYWORDS
tkeqcut, segymerge, qmerge
.SH SEE ALSO
segymerge & qmerge
.SH AUTHOR
Bruce Beaudoin (bruce@passcal.nmt.edu)
