=======
tkeqcut
=======

* Description: Prepare input for either segymerge or qmerge

* Usage: tkeqcut

* Free software: GNU General Public License v3 (GPLv3)
