=======
History
=======

2008.180 (2018-06-07)
------------------

* First release on new build system.

2020.216 (2020-08-03)
------------------
* Updated to work with Python 3
* Added a unit test to test tkeqcut import
* Updated list of platform specific dependencies to be installed when pip
  installing tkeqcut (see setup.py)
* Installed and tested tkeqcut against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda package for tkeqcut that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline

2022.1.0.0 (2022-01-11)
------------------
* New versioning scheme
