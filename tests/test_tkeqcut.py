#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `tkeqcut` package."""

import unittest

from unittest.mock import patch
from tkeqcut.tkeqcut import main


class TestTkeqcut(unittest.TestCase):
    """Tests for `tkeqcut` package."""

    def test_import(self):
        """Test tkeqcut import"""
        with patch("sys.argv", ["tkeqcut", "-#"]):
            with self.assertRaises(SystemExit) as cmd:
                main()
            self.assertEqual(cmd.exception.code, 1, "sys.exit(1) never called "
                             "- Failed to exercise tkeqcut")
